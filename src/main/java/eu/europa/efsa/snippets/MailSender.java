package eu.europa.efsa.snippets;


import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailSender {
	public static void main(String[] args) {
        String from = "noreply.does.not.exist@efsa.europa.eu";
        String to = "Diego.PASCOTTO@ext.efsa.europa.eu";
        String subject = "Hi There...";
        String text = "How are you?";
 
        //
        // A properties to store mail server SMTP information such 
        // as the host name and the port number. With this properties 
        // we create a Session object from which we'll create the 
        // Message object.
        //

        // https://mail-relay.efsa.test/owa
 
 
        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", "mail-relay.efsa.test");
            props.put("mail.smtp.port", "25");
            props.put("mail.smtp.auth", "true");
            props.put("mail.transport.protocol", "smtp");
            //props.put("mail.smtp.starttls.enable", "true");
 
            Authenticator auth = new SMTPAuthenticator();
            Session mailSession = Session.getDefaultInstance(props, auth);
            // uncomment for debugging infos to stdout
            mailSession.setDebug(true);
            Transport transport = mailSession.getTransport();

            MimeMessage message = new MimeMessage(mailSession);
            message.setContent("This is a test", "text/plain");
            //message.setFrom(new InternetAddress("noreply-does-not-exist@efsa.europa.eu"));
            //message.setFrom(new InternetAddress("Giovanni.SILVESTRI@efsa.test", "Your Reporting Officer"));
            message.setFrom(new InternetAddress("NoReply@efsa.test", "Your Reporting Officer"));
            
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("Diego.PASCOTTO@ext.efsa.europa.eu"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("Giovanni.SILVESTRI@efsa.test"));

            transport.connect();
            transport.sendMessage(message,  message.getRecipients(Message.RecipientType.TO));
            transport.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	private static class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
           String username = "test\\silvegi";
           String password = "Ab123456";
           return new PasswordAuthentication(username, password);
        }
    }
}
