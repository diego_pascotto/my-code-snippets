package eu.europa.efsa.snippets.ews.buckett;

import java.net.URI;
import java.net.URISyntaxException;

import microsoft.exchange.webservices.data.EmailMessageSchema;
import microsoft.exchange.webservices.data.ExchangeCredentials;
import microsoft.exchange.webservices.data.ExchangeService;
import microsoft.exchange.webservices.data.ExchangeVersion;
import microsoft.exchange.webservices.data.FindItemsResults;
import microsoft.exchange.webservices.data.Item;
import microsoft.exchange.webservices.data.ItemView;
import microsoft.exchange.webservices.data.SearchFilter;
import microsoft.exchange.webservices.data.WebCredentials;
import microsoft.exchange.webservices.data.WellKnownFolderName;

public class Client {
	
	private static final String MY_PASSWORD_TEST = "Ab123456";
	private ExchangeService service;
	private static String MY_PASSWORD = "Repubblic4";

	public static void main(String[] args) {
//		Client client = new Client();
//		
//		try {
//			client.getEMailBySubject("LRW");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		testEWSClient();
	}

	private void getEMailBySubject(String subj) throws Exception {
		SearchFilter filter = new SearchFilter.ContainsSubstring(EmailMessageSchema.Subject, subj);
		ItemView view = new ItemView(20);
		
		ExchangeService theService = getService();
		
		FindItemsResults<Item> results = theService.findItems(WellKnownFolderName.Inbox, filter, view);
		for (Item item : results) {
			System.out.println(item.getSubject());
		}
		
	}

	public ExchangeService getService() throws URISyntaxException {
		if (service == null) {
			service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
			//ExchangeCredentials credentials = new WebCredentials("pascodi", "Repubblic4");
			//ExchangeCredentials credentials = new WebCredentials("test\\silvegi", "Ab123456");
			//ExchangeCredentials credentials = new WebCredentials("silvegi", "Ab123456", "TEST");
			//ExchangeCredentials credentials = new WebCredentials("SVC-Appointments", "Ab123456");
			ExchangeCredentials credentials = new WebCredentials("maildispatcher", "Ab123456");
			//ExchangeCredentials credentials = new WebCredentials("Diego.PASCOTTO@ext.efsa.europa.eu", "Repubblic4");
			service.setCredentials(credentials);
			//service.setUseDefaultCredentials(false);
			
			service.setUrl(new URI("https://mail.efsa.europa.eu/ews"));
			//service.setUrl(new URI("https://mail-relay.efsa.test/ews/exchange.asmx"));
			service.setTraceEnabled(true);
		}
		return service;
	}
	
	public static void testEWSClient() {
		try {
			ExchangeService exchangeService = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
			//ExchangeCredentials credentials = new WebCredentials("pascodi", MY_PASSWORD);
			//ExchangeCredentials credentials = new WebCredentials("EFSA\\pascodi", MY_PASSWORD);
			ExchangeCredentials credentials = new WebCredentials("EFSA\\pascodi.dev", MY_PASSWORD_TEST);
			exchangeService.setCredentials(credentials);
			//exchangeService.setUrl(new URI("https://mail.efsa.europa.eu/ews"));
			//exchangeService.setUrl(new URI("https://mail.efsa.europa.eu/ews/exchange.asmx"));
			exchangeService.setUrl(new URI("https://mail-relay.efsa.test/ews/exchange.asmx"));
			exchangeService.setTraceEnabled(true);

			SearchFilter filter = new SearchFilter.ContainsSubstring(EmailMessageSchema.Subject, "LRW");
			ItemView view = new ItemView(20);
			
			FindItemsResults<Item> results = exchangeService.findItems(WellKnownFolderName.Inbox, filter, view);
			for (Item item : results) {
				System.out.println(item.getSubject());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
