package eu.europa.efsa.snippets.newcomers.test;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.text.GapContent;

public class TestOldVsNew {
	static DaoNew nDao = new DaoNew();
	static DaoOld oDao = new DaoOld();

	public static void main(String[] args) {
		
		for (int y = 2000; y <= 2013; y ++) {
			for (int m = 1; m <= 12; m ++) {
				compare(y, m);
			}
		}
		System.out.println("Test completed");

	}
	
	public static String getYyyyMm(int y, int m) {
		String yyyyMM = "" + y + "-";
		if ( m < 10) yyyyMM += "0";
		yyyyMM += m;
		return yyyyMM;
	}

	private static void compare(int y, int m) {
		List<Employee> newcomersOldStyle = oDao.getNewcomersByMonth(y, m);
		List<Employee> newcomersNewStyle = nDao.getNewcomersByMonth(y, m);
		
		//System.out.println("Begin comparison for " + y + " / " + m);
		
		int o = (newcomersOldStyle == null ? 0 : newcomersOldStyle.size());
		int n = (newcomersNewStyle == null ? 0 : newcomersNewStyle.size());
//		System.out.println("Number of newcomers from OLD query = " + o);
//		System.out.println("Number of newcomers from NEW query = " + n);
		
		if (o != n) {
			System.out.println("Match (maybe) failed for " + y + " / " + m);
			seeDiff(newcomersOldStyle, newcomersNewStyle, getYyyyMm(y, m));
		}
	}

	private static void seeDiff(List<Employee> newcomersOldStyle, List<Employee> newcomersNewStyle, String yyyyMM) {
		HashMap<String, Object> oMap = createMap(newcomersOldStyle);
		HashMap<String, Object> nMap = createMap(newcomersNewStyle);

		match("\t\told on new: ", oMap, nMap, yyyyMM);
		match("\t\tnew on old: ", nMap, oMap, yyyyMM);
	}

	private static void match(String string, HashMap<String, Object> map1, HashMap<String, Object> map2, String yyyyMM) {
		Set keys1 = map1.keySet();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		for (Iterator it1 = keys1.iterator(); it1.hasNext(); ) {
			String key1 = (String)it1.next();
			Employee emp1 = (Employee)map1.get(key1);
			boolean found = (map2.get(key1) != null);
			if (!found) {
				if (emp1.toDate != null) {
					String toDate = sdf.format(emp1.toDate);
					if (yyyyMM.equals(toDate)) {
						System.out.println(string + "Ignoring non-match for " + emp1.aliasName + " (to-date same as from date)");
						continue;
					}
				}
				if (emp1.termDate != null) {
					String termDate = sdf.format(emp1.termDate);
					if (yyyyMM.equals(termDate)) {
						System.out.println(string + "Ignoring non-match for " + emp1.aliasName + " (termDate-date same as from date)");
						continue;
					}
				}
				if (emp1.termDate != null && emp1.toDate != null && emp1.termDate.after(emp1.toDate)) {
					System.out.println(string + "Ignoring non-match for " + emp1.aliasName + " (inconsistent: termDate-date > to-date)");
					continue;
				}
				if (emp1.gapInDays > 0) {
					System.out.println(string + "Ignoring non-match for " + emp1.aliasName + " (gap > 0)");
					continue;
				}
				System.err.println(string + " - match not found for " + emp1.employeeNumber + " - " + emp1.aliasName + " (" + emp1.fromDate + " / " + emp1.termDate + " / " + emp1.toDate + "), gap = " + emp1.gapInDays);
			}
		}
	}

	private static HashMap<String, Object> createMap(List<Employee> list) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		for (Employee emp : list) {
			map.put(emp.employeeNumber, emp);
		}
		return map;
	}

}
