package eu.europa.efsa.snippets.newcomers.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DaoOld extends Dao {
	public List<Employee> getNewcomersByMonth(int y, int m) {
		String yyyyMM1 = "" + y + "-";
		if ( m < 10) yyyyMM1 += "0";
		yyyyMM1 += m;
		
		int mPrev = -1;
		int yPrev = y;
		if (m != 1) {
			mPrev = m - 1;
		} else {
			mPrev = 12;
			yPrev = y - 1;
		}
		String yyyyMM2 = "" + yPrev + "-";
		if ( mPrev < 10) yyyyMM2 += "0";
		yyyyMM2 += mPrev;
		
		Connection connection = getAllegroConnection();
		PreparedStatement statement = null;
		ResultSet rset = null;
		List<Employee> list = null;
		try {
			statement = connection.prepareStatement(
				"SELECT E.EMPLOYEE_ID, E.ALIAS_NAME, E.USUAL_LAST_NAME, E.USUAL_FIRST_NAME, C.CODE, C.FROM_DATE, C.RESIGNATION_DATE, C.TO_DATE " +
				" FROM EFSA_V_EMPLOYEE E, EFSA_V_EMPLOYEE_CONTRACT C " +
				" WHERE  " +
				"   E.EMPLOYEE_ID = C.EMPLOYEE_ID AND " +
				"   CONVERT(CHAR(7), C.FROM_DATE, 120) = ?   " +
				"   AND C.CODE IN ('CA','CAP','CNS','EXT','INT','NPT','OFF','OFP','SNE','TA','TAP','TRN','CNS','AUX') " +
				"   AND C.EMPLOYEE_ID NOT IN  " +
				"   ( SELECT DISTINCT " +
				"                               EC1.EMPLOYEE_ID " +
				"                           FROM " +
				"                               EFSA_V_EMPLOYEE_CONTRACT EC1 " +
				"                           WHERE " +
				"                               ( " +
				"                                   CONVERT(CHAR(7), EC1.TO_DATE, 120) = ? " +
				"                               OR  CONVERT(CHAR(7), EC1.TO_DATE, 120) = ? ) ) " +
				"   AND C.EMPLOYEE_ID NOT IN  " +
				"   (  " +
				"         SELECT DISTINCT   " +
				"         EC2.EMPLOYEE_ID   " +
				"        FROM   " +
				"         EFSA_V_EMPLOYEE_CONTRACT EC2   " +
				"        WHERE   " +
				"                 (  " +  
				"                         CONVERT(CHAR(7), EC2.RESIGNATION_DATE, 120) = ?  " +  
				"                         OR  CONVERT(CHAR(7), EC2.RESIGNATION_DATE, 120) = ? )  " +
				"   )");
			statement.setString(1, yyyyMM1);
			statement.setString(2, yyyyMM1);
			statement.setString(3, yyyyMM2);
			statement.setString(4, yyyyMM1);
			statement.setString(5, yyyyMM2);
			
			rset = statement.executeQuery();
			
			list = new ArrayList<Employee>();
			while (rset.next()) {
				Employee emp = new Employee();
				
				emp.startYearAsNewComer = yyyyMM1;
				emp.employeeNumber = rset.getString("EMPLOYEE_ID");
				emp.aliasName = rset.getString("ALIAS_NAME");
				emp.fromDate = rset.getDate("FROM_DATE");
				emp.termDate = rset.getDate("RESIGNATION_DATE");
				emp.toDate = rset.getDate("TO_DATE");
				
				list.add(emp);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
}
