package eu.europa.efsa.snippets.newcomers.test;

import java.sql.Connection;
import java.sql.DriverManager;

public class Dao {
	public Connection getAllegroConnection() {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			connection = DriverManager.getConnection("jdbc:sqlserver://grenoble.efsa.test;databaseName=allegro_db", "allegro_user", "Ab123456!");
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return connection;
	}
}
