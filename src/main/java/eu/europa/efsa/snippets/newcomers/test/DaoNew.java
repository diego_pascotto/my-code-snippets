package eu.europa.efsa.snippets.newcomers.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DaoNew extends Dao {
	public List<Employee> getNewcomersByMonth(int y, int m) {
		String yyyyMM = TestOldVsNew.getYyyyMm(y, m);
		
		Connection connection = getAllegroConnection();
		PreparedStatement statement = null;
		ResultSet rset = null;
		List<Employee> list = null;
		try {
			statement = connection.prepareStatement(
					"select * from EFSA_VIEWS_SCHEMA.EFSA_V_NEWCOMER " +
					"where START_YEAR_MONTH_AS_NEWCOMER = ? " +
					"and CURR_CONTR_TYPE in ('CA','CAP','CNS','EXT','INT','NPT','OFF','OFP','SNE','TA','TAP','TRN','CNS','AUX')");
			statement.setString(1, yyyyMM);
			
			rset = statement.executeQuery();
			
			list = new ArrayList<Employee>();
			while (rset.next()) {
				Employee emp = new Employee();
				
				emp.startYearAsNewComer = yyyyMM;
				emp.employeeNumber = rset.getString("EMPLOYEE_ID");
				emp.aliasName = rset.getString("ALIAS_NAME");
				emp.fromDate = rset.getDate("CURR_CONTR_START_DATE");
				emp.termDate = rset.getDate("CURR_CONTR_TERM_DATE");
				emp.toDate = rset.getDate("CURR_CONTR_END_DATE");
				emp.gapInDays = rset.getInt("GAP_IN_DAYS");
				
				list.add(emp);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
