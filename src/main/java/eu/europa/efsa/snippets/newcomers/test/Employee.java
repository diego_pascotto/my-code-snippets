package eu.europa.efsa.snippets.newcomers.test;

import java.io.Serializable;
import java.sql.Date;

public class Employee implements Serializable {

	public String startYearAsNewComer;
	public String employeeNumber;
	public String aliasName;
	public Date fromDate;
	public Date termDate;
	public Date toDate;
	public int gapInDays;

}
