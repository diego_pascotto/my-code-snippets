package eu.europa.efsa.snippets.log4j.jmx;

import java.util.Enumeration;

import org.apache.log4j.Appender;
import org.apache.log4j.Category;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import eu.europa.efsa.snippets.log4j.standalone.MyClass;


public class Log4jConfigurator implements Log4jConfiguratorMBean {

	@Override
	public void setLogLevel(String loggerName, String newLevel) {
		System.out.println("Requested logging level " + newLevel + " for logger " + loggerName);
		
		Logger log = Logger.getLogger(loggerName);
		
		if (log != null) {
			MyClass.printLoggerInfo(log);
		
			try {
				Level lev = Level.toLevel(newLevel);
				System.out.println("Setting new log level to " + lev.toString());
				
				log.setLevel(lev);
			} catch (Exception e) {
				System.err.println("Something's wrong with " + newLevel);
				e.printStackTrace();
			}
			
		} else {
			System.out.println("Logger " + loggerName + " does not exist");
		}
	}

	@Override
	public void listAllLoggers() {
		Enumeration<Category> loggers = LogManager.getCurrentLoggers();
		
		System.out.println("==== LOG4J current loggers ====");
		while (loggers.hasMoreElements()) {
			Category cat = loggers.nextElement();
			System.out.println("Logger: " + cat.getName() + " - " + cat.getLevel());
			System.out.println("-- Appenders --");
			for (Enumeration en = cat.getAllAppenders(); en.hasMoreElements(); ) {
				Appender app = (Appender)en.nextElement();
				if (app instanceof FileAppender) {
					System.out.println(app.getName() + " - " + ((FileAppender)app).getFile());
				} else {
					System.out.println(app.getName());
				}
				System.out.println("---------------");
			}
			
			System.out.println("-------------------------------");
		}
		System.out.println("===============================");
		
	}

}
