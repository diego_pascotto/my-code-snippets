package eu.europa.efsa.snippets.log4j.standalone;

import java.util.Enumeration;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import eu.europa.efsa.snippets.log4j.standalone.package1.MyClass1;
import eu.europa.efsa.snippets.log4j.standalone.package2.MyClass2a;
import eu.europa.efsa.snippets.log4j.standalone.package2.MyClass2b;

public class MyClass {

	private Logger log;
	
	public static void main(String[] args) {
		MyClass aClassOfMine = new MyClass();
		
		//aClassOfMine.doSomeLogging();
		aClassOfMine.doSomeMoreComplexLogging();
	}

	public MyClass() {
		super();
		
		System.out.println("log4j.defaultInitOverride = " + System.getProperty("log4j.defaultInitOverride"));
		
		//PropertyConfigurator.configure("D:/javasource/efsa/code-snippets/src/main/resources/log4j.properties");
		PropertyConfigurator.configure("D:/javasource/efsa/code-snippets/src/main/resources/log4j-multilevel.properties");
		
		log = Logger.getLogger(MyClass.class);
		
	}

	private void doSomeLogging() {
		printLoggerInfo();
		
		log.debug("Hello world");
	}
	
	private void doSomeMoreComplexLogging() {
		printLoggerInfo();
		printLoggerInfo(MyClass1.log);
		printLoggerInfo(MyClass2a.log);
		printLoggerInfo(MyClass2b.log);
		
		MyClass1.logSomeStuff();
		MyClass2a.logSomeStuff();
		MyClass2b.logSomeStuff();
	}
	
	public static void printLoggerInfo() {
		printLoggerInfo(Logger.getRootLogger());
	}
	
	public static void printLoggerInfo(Logger aLog) {
		System.out.println("=================== LOGGING CONFIGURATION ===================");
		System.out.println("name = " + aLog.getName());
		System.out.println("-------------------------------------------------------------");
		Level level = aLog.getLevel();
		System.out.println("level = " + level);
		System.out.println("---- Appenders ----");
		System.out.println("-------------------");
		for (Enumeration en = aLog.getAllAppenders(); en.hasMoreElements(); ) {
			Appender app = (Appender)en.nextElement();
			System.out.println("name ....... " + app.getName());
			if (app instanceof FileAppender) {
				System.out.println("file ....... " + ((FileAppender)app).getFile());
			}
			System.out.println("-------------------");
		}
		System.out.println("=============================================================");
	}


}


