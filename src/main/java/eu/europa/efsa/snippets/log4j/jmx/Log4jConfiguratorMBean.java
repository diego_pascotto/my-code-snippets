package eu.europa.efsa.snippets.log4j.jmx;

public interface Log4jConfiguratorMBean {
	public void listAllLoggers();
	public void setLogLevel(String loggerName, String newLevel);
}
