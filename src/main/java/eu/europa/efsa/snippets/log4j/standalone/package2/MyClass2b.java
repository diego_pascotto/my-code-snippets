package eu.europa.efsa.snippets.log4j.standalone.package2;

import org.apache.log4j.Logger;

public class MyClass2b {
	public static Logger log = Logger.getLogger(MyClass2b.class);
	
	public static void logSomeStuff() {
		log.fatal("---- package2.MyClass2b ----");
		log.debug("DEBUG");
		log.info ("INFO");
		log.warn ("WARN");
		log.error("ERROR");
		log.fatal("FATAL");
		log.fatal("----------------------------");
	}
}
