package eu.europa.efsa.snippets.log4j.standalone.package1;

import org.apache.log4j.Logger;

public class MyClass1 {
	public static Logger log = Logger.getLogger(MyClass1.class);
	
	public static void logSomeStuff() {
		log.fatal("---- package1.MyClass1 ----");
		log.debug("DEBUG");
		log.info ("INFO");
		log.warn ("WARN");
		log.error("ERROR");
		log.fatal("FATAL");
		log.fatal("---------------------------");
	}
}
