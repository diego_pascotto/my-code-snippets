package eu.europa.efsa.snippets.log4j.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

import eu.europa.efsa.snippets.log4j.standalone.MyClass;

public class Log4jServlet extends HttpServlet {
	
	Logger log = null; // Moved to init() to make the log4j.debug effective BEFORE the creatiorn of the logger

	@Override
	public void init() throws ServletException {
		super.init();
		
		/*
		 * This causes log4j to log (on standard output) detailed info
		 * about from where the configuration is loaded etc.
		 */
		System.out.println("< System.out.println > Setting log4j.debug property....");
		System.setProperty("log4j.debug", "true"); // Value doesn't matter
		
		log = Logger.getLogger(Log4jServlet.class);
		MyClass.printLoggerInfo();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGetOrPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGetOrPost(req, resp);
	}

	private void doGetOrPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("< System.out.println > Welcome to Log4jServlet!");
		
		String customAction = req.getParameter("myAction");
		System.out.println("< System.out.println > customAction = " + customAction);
		
		if ("logIt".equalsIgnoreCase(customAction)) {
			String message = req.getParameter("message");
			
			log.debug("< log.debug > " + message);
			log.info ("< log.info  > " + message);
			log.warn ("< log.warn  > " + message);
			log.error("< log.error > " + message);
			log.fatal("< log.fatal > " + message);
			log.trace("< log.trace > " + message);
			
		} else if ("reset".equalsIgnoreCase(customAction)) {
			System.out.println("< System.out.println > Requested configuration reset");
				
			reset(req, resp);
		} else if ("print".equalsIgnoreCase(customAction)) {
			MyClass.printLoggerInfo();
		} else {
			throw new ServletException("Custom action " + customAction + " not recognized");
		}
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/log4j-home.html");
		dispatcher.forward(req, resp);
	}

	private void reset(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getParameter("filename");
		String delay = req.getParameter("delay");
		System.out.println("< System.out.println > new configuration file = " + path);
		System.out.println("< System.out.println > delay = " + delay);
		
		LogManager.resetConfiguration();
		
		long ldelay = 0;
		if (delay != null && !"0".equals(delay)) {
			ldelay = Long.parseLong(delay);
		}
		
		if (path.endsWith(".xml")) {
			if (ldelay != 0) {
				System.out.println("< System.out.println > using DOMConfigurator with WATCH option");
				DOMConfigurator.configureAndWatch(path, ldelay);
			} else {
				System.out.println("< System.out.println > using DOMConfigurator");
				DOMConfigurator.configure(path);
			}
		} else {
			if (ldelay != 0) {
				System.out.println("< System.out.println > using PropertyConfigurator with WATCH option");
				PropertyConfigurator.configureAndWatch(path, ldelay);
			} else {
				System.out.println("< System.out.println > using PropertyConfigurator");
				PropertyConfigurator.configure(path);
			}
		}
		
		System.out.println("< System.out.println > configurator reset, reloading logger");
		log = Logger.getLogger(Log4jServlet.class);
		System.out.println("< System.out.println > configurator reset, logger reloaded");
		log.debug("< log.debug > configuration reset");
		
		MyClass.printLoggerInfo();
	}
	



}
