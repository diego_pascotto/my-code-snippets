package eu.europa.efsa.snippets.cxf;

import javax.xml.ws.Endpoint;


public class StartClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MyEmpView impl = new MyEmpView();
		Endpoint.publish("http://localhost:8081/MyEmpView", impl);
		
		while (true) {
			try {
				System.out.println("Listening...");
				Thread.currentThread().sleep(30* 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}
