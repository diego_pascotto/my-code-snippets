package eu.europa.efsa.snippets.cxf;

import java.util.ArrayList;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import eu.europa.efsa.snippets.cxf.vo.Employee;

@WebService(name = "MyEmpView", targetNamespace = "http://snippets.efsa.europa.eu")
@SOAPBinding(style = Style.RPC)
public class MyEmpView {
	
	public ArrayList<Employee> getEmpsByAliases(@WebParam(name = "aliases") String[] aliases) {
		ArrayList<Employee> list = new ArrayList<Employee>();
		for(String alias: aliases) {
			System.out.println("Alias: " + alias);
			
			Employee emp = new Employee();
			
			emp.setSurname(alias.substring(0, 5) + "xxx");
			emp.setName(alias.substring(5) + "yyy");
			
			list.add(emp);
		}
		
		System.out.println("_________________________");
		for (Employee e : list) {
			System.out.println(e.getName() + " - " + e.getSurname());
		}
		return list;
	}
	
	public Employee[] getEmpsByAliases2(@WebParam(name = "aliases") String[] aliases) {
		Employee[] list = new Employee[10];
		int count = 0;
		for(String alias: aliases) {
			System.out.println("Alias: " + alias);
			
			Employee emp = new Employee();
			
			emp.setSurname(alias.substring(0, 5) + "xxx");
			emp.setName(alias.substring(5) + "yyy");
			
			list[count] = emp;
			count++;
		}
		return list;
	}

}
