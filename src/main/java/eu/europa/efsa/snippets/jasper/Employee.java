package eu.europa.efsa.snippets.jasper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class Employee implements Serializable {
	
	private String displayName;
	private String contractType;
	
	
	
	public Employee() {
	}

	public Employee(String displayName, String contractType) {
		super();
		this.displayName = displayName;
		this.contractType = contractType;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	
	public static Collection<Employee> getEmployeeList() {
		
		ArrayList<Employee> list = new ArrayList<Employee>();
		
		list.add(new Employee("Snow WHITE", "good"));
		list.add(new Employee("Prince CHARMING", "good"));
		
		return list;
		
	}
	
	public static Collection getEmployeeList2() {
		
		ArrayList list = new ArrayList();
		
		list.add(new Employee("Snow WHITE", "good"));
		list.add(new Employee("Prince CHARMING", "good"));
		
		return list;
		
	}
	

}
