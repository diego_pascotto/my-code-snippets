package eu.europa.efsa.snippets.jasper;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class JasperServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("[JasperServlet::doGet] BEGIN");
		String id = req.getParameter("reportId");
		
		if ("1".equals(id))
			doReport1(req, resp);
		else if ("2".equals(id))
			doReport2(req, resp);
		else
			throw new ServletException("Report type " + id + " not known");
		
	}

	private void doReport1(HttpServletRequest req, HttpServletResponse resp) {
		try {
			
			resp.setContentType("application/pdf");
	        InputStream is = getClass().getResourceAsStream("/efsa-users.jasper");
	        
	        Map<String, Object> emptyMap = new HashMap();
	        Connection connection = null;
	        Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager.getConnection("jdbc:oracle:thin:@(description=(address_list=(load_balance=on)(failover=on)(address=(protocol=tcp)(host=dbtst011-vip.efsa.eu.int)(port=1521))(address=(protocol=tcp)(host=dbtst012-vip.efsa.eu.int)(port=1521)))(connect_data=(service_name=APPLTEST)))", 
												"USERAPPS", "Us43_pps2");
			
			JasperRunManager.runReportToPdfStream(is, resp.getOutputStream(), emptyMap, connection);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("[JasperServlet::doGet] END");
		}
	}
	
	private void doReport2(HttpServletRequest req, HttpServletResponse resp) {
		try {
			
			resp.setContentType("application/pdf");
	        InputStream is = getClass().getResourceAsStream("/java-beans-ds.jasper");

	        Map<String, Object> emptyMap = new HashMap();
	        Collection<Employee> list = new ArrayList<Employee>();
	        
	        for (int i = 1; i <= 200; i++)
	        	list.add(new Employee("Diego Pascotto " + i, "Consultant " + i));
	        
	        JasperRunManager.runReportToPdfStream(is, resp.getOutputStream(), emptyMap, new JRBeanCollectionDataSource(list));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("[JasperServlet::doGet] END");
		}
	}

}
